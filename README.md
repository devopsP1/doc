# Créer un mod sur Minetest

Rendez-vous sur https://garagenum.gitlab.io/web/mtmod pour voir le tutoriel

# Pour modifier le tutoriel

1. Forkez ce projet
2. Installez les dépendances du projet sur le système:
  ```
  git nodejs npm wget python3
  ```
3. Seulement si nécessaire, ré-installez les librairies du projet avec:
  ```
  bash build.sh
  ```
4. Déployez le serveur avec:
  ```
  bash deploy.sh
  ```
5. Accédez au contenu à l'adresse http://localhost:8000
6. Modifiez le fichier montuto.md
7. Arrêtez le serveur avec:
  ```
  pkill python3
  ```
8. pushez, vous avez en ligne une présentation faite avec reveal.js
