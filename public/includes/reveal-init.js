(function(){

  Reveal.initialize({
    history : true,
    dependencies : [

        // Cross-browser shim that fully implements classList - https://github.com/eligrey/classList.js/
        { src:'/build/classList.min.js', condition: function() { return !document.body.classList; } },

        // Interpret Markdown in <section> elements
        { src:'/build/reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
        { src:'/build/reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },

        // Syntax highlight for <code> elements
        {
          src:'/build/reveal.js/plugin/highlight/highlight.pack.js',
          async: true,
          callback: function() {
            hljs.initHighlightingOnLoad();
          }
        },

        // Zoom in and out with Alt+click
        { src:'/build/reveal.js/plugin/zoom-js/zoom.js', async: true },

        //notes
        { src:'/build/reveal.js/plugin/notes/notes.js', async: true },
        
        //maths
        { src:'/build/reveal.js/plugin/math/math.js', async: true }//,

        //pdf
      //  { src: '/build/reveal.js/plugin/print-pdf/print-pdf.js', async: true}
    ]
  });
})();
