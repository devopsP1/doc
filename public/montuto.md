# Topo de ce que l'on a fait depuis le 6/01/2020


***


## Commande Linux à apprendre par coeur 
### Partie 1: Commande importante ! 

#Commande de base que l'on a appris

```
cd      : pour ce déplacer dans les dossier
cd ..   : pour retourner en arrière 
cd ~    : mène au répertoire de l'utilisateur courant 
ls      : listes les fichiers
ls -a   : afficher les fichiers cacher
ls -r   : affiche les sous dossiers
ls -h   : 
clear   : Nettoyoie le terminal
cp      : copier d'un fichier
cp -R   :  permet de réaliser des copie des dossier entier
mv      : pour déplacer un fichier
mkdir   : permet de crée un dossier
touch   : permet de crée un fichier
cat     : permet de lire le contenue d'un fichier
less    : affiche le fichier page par page
rm      : supprime les fichiers
ssh     : permet de se connecter au shell d'un ordinateur distant et d'y exécuter des commandes
sort    : trier les fichiers par ordre alphabétique 
sort -r : permet de trier dans l'ordre anti-alphabétique
sort -u : permet d'éliminer les doublons 
cut     : permet de couper un fichier texte
file    : fichier



```

# installer flatpak pour avoir la dernière version de Minetest

    sudo apt update
    sudo apt install flatpak geany

# On quitte l'admin avec **CTRL + D**


***


### Partie 2: en tant qu'utilisateur  

```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub net.minetest.Minetest
export MT=.var/app/net.minetest.Minetest/.minetest
```

***


## Installer le jeu du Garage Numérique  
```
cd $MT
mkdir games
cd games
git clone https://gitlab.com/garagenum/minetest-lug9000
```

***


## Créer un projet de mod Minetest
1. Créer un compte sur gitlab.com
2. cliquer sur Nouveau Projet


***


## Cloner le projet sur son ordinateur  
Revenir dans le terminal et écrire:
```
cd $MT 
mkdir mods && cd mods 
git clone https://gitlab.com/mon-pseudo/monprojet.git
cd monprojet
```


***


## Créer le fichier init.lua
```
geany init.lua&
```	

***


Tu peux éditer le fichier init.lua en t'aidant de 
- [le livre écrit par RubenWardy](https://rubenwardy.com/minetest_modding_book/en/index.html)  
- [la doc officielle](https://github.com/minetest/minetest/blob/master/doc/lua_api.txt)
