#!/bin/bash
rm -Rfv public/build
mkdir -p public/build
cd public/build
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js
rm -r .git*
npm install
wget https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js
wget https://github.com/eligrey/classList.js/blob/master/classList.min.js
cd ../../..
