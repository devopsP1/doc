#!/bin/bash
mkdir -p archives
cd public
FILE=$(ls *.md | grep -v "README.md")
ARCHIVE=`date '+_%Y-%m-%d_%H:%M'`
cp $FILE ../archives/$FILE$ARCHIVE -vf
cd includes
ln -fs ../$FILE
cd ..
ln -fs includes/index.html
pkill python3
python3 -m http.server&
